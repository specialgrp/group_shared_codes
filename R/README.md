## R folder
This folder (R/) contains snippets of codes in R.

### interpolate_autoregression_conserve_mean.R
This script contains a function which performs an autoregressive interpolation while conserving the mean of the time period. The function also allows for an option for minimum and maximum bounds as well which the spline option does not offer. The algorithm was taken from Rymes and Myers (2001), but a few equations were altered by Kamolphat Atsawawaranunt in order to get the algorithm to work. If use, please cite:

* *Rymes, M.D. and Myers, D.R., 2001. Mean preserving algorithm for smoothly interpolating averaged data. Solar Energy, 71(4), pp.225-231. doi: 10.1016/S0038-092X(01)00052-4*

and please contact **Kamolphat Atsawawaranunt (k.atsawawaranunt@reading.ac.uk)**

### interpolate_spline_conserve_mean.R
This script contains a function which performs spline interpolation while conserving the mean of the time period. **Please note that the current function do not allow limits on the interpolation curve; i.e. in cases of precipitation where the minimum value cannot be less than 0**

Contact: **Kamolphat Atsawawaranunt (k.atsawawaranunt@reading.ac.uk)**

### pollen_function_script.R
This script contains functions which are used to deal with pollen data. The scripts includes the following functions:

 * check_duplicated - this function check that the taxon cleaner does not have duplicated names in the taxon cleaner (when amalgamating, that there is only one name it is amalgamated to)
 * check4spaces - this function looks for whitespaces before/after the name of everything in the vector/list. This also includes special whitespace characters
 * convert2Rnames - this function essentially replaces all spaces ' ' with '.' in all strings in a vector/list
 * unassigned_taxa_mat - this function checks for unassigned taxa in the data matrix (rows = samples, columns = taxon names) which are not found in the taxon cleaner. Unassigned taxa are appended to the taxon cleaner table.
 * unassigned_taxa_tab - this function checks for unassigned taxa in the data table (format as outputed from database) which are not found in the taxon cleaner. Unassigned taxa are appended to the taxon cleaner table.
 * check_input_all_numbers - this function checks that all the columns (with specified exceptions) in the dataframe are numbers or convertable to numbers. If not, the column name and the row numbers are specified.
 * clean_matrix - this function clean the data matrix (rows = samples, columns = taxon names) by converting all counts columns to numeric (if possible), remove zero-sum columns (i.e. taxa with no counts) and zero-sum rows (i.e. samples with no counts)
 * amalgamate_matrix - this function amalgamates the data matrix (rows = samples, columns = taxon names) based on the taxon cleaner and returns a data matrix with the new counts of each amalgamated taxa summed up.
 * amalgamate_table - this function amalgamates the data table (format as outputed from database) based on the taxon cleaner and return a data matrix with new counts of each amalgamated taxa summed up.
 * convertRnames2std - this function converts R names back to standard names based on a table (i.e. output from convert2Rnames function).

Contact: **Kamolphat Atsawawaranunt (k.atsawawaranunt@reading.ac.uk)**

### t_model.R
This script contains the T model function which calculates increment of diameter from GPP and specific parameters. If use, please cite:

* *Li, G., Harrison, S. P., Colin Prentice, I., & Falster D. 2014. Simulation of tree ring-widths with a model for primary production, carbon allocation and growth. Biogeosciences 11, 6711-6724. doi: 10.5194/bg-11-6711-2014*

Contact: **Dr. Guangqi Li (g.li2@reading.ac.uk)**