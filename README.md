# group_shared_codes

This repository contains snippets of codes that group members in the SPECIAL lab group may find useful. This README provides the overview of the entire repository. For more details for each script, please see the README within the respective folders.

## Python folder
**calc_annual_from_months_standard.py** - annual mean or annual total from monthly netCDF file.

**convert2minus180to180.py** - converts netCDF files which are in 0 to 360 degrees format to -180 to 180 degrees format.

**extract_netCDF_shapefiles.py** - crops netCDF file based on a shape file.

**extract_timeseries.py** - area-weighted mean time series (ignoring NA) from netCDF file (base on the latitudes). 

**extractvar2points.py** - extract values from netCDF file based on latitude and longitude.

**MAT_from_GDD0_MTCM_function.py** - Mean Annual Temperature (MAT) from Growing degree days 0 degrees C (GDD0) and Mean temperature of the coldest month (MTCM).

**monthly_clim_calc.py** - monthly climatology netCDF file (based on a time period) from monthly netCDF file.

**interpolate_autoregression_conserve_mean.py** - mean conserving autoregressive interpolation (R version available).

**interpolate_spline_conserve_mean.py** - mean conserving spline interpolation (R version available).

**interpolate_netCDF.py** - interpolates/regrid netCDF files.

**spatialglm_timeperiod.py** - GLM on every grid cell.

**splash_wrapper subfolder** - splash wrapper for running SPLASH on CSV files. Contain scripts from SPLASH as well.

**MI_adjuster.py** - correct pollen based modern analogue reconstructions of moisture index for changes in atmospheric [CO_2]

## R folder
**interpolate_autoregression_conserve_mean.R** - mean conserving autoregressive interpolation (R version available).

**interpolate_spline_conserve_mean.R** - mean conserving spline interpolation (R version available).

**pollen_function_script.R** - functions used to deal with pollen data (check for duplicated names, unassigned taxa and taxa amalgamation).

**t_model.R** - functions to calculate increment of diameter from GPP and other specific parameters.

***To be added to the repository. This list is only temporary and added here as a reminder for A. 
overallglm_timeperiod.py - similar to spatialglm_timeperiod but instead glm is performed every year instead.
vol_ref_lef_diff.py - create relative difference netCDF file, relative to a particular year. Works on annual netCDF file with year CE as the time variable.
mean_timeperiod.py - create netCDF file as a mean of a time period (works on annual netCDF, assuming each year is weighted equally and year CE as the time variable).
REOF_standardisation.ncl - create netCDF file and output png result from REOF. Results were never used but it does seem to work. There are examples online but I had to alter a fair bit to get it going.
p_model - version that calculates p_model based on PPFD from SWdown as to using estimates from SPLASH
list of useful nco operators which are often available on linux machines.
GLSDBv2 analysis scripts (z-scores standardisation, etc.)
MI with CO2 correction from Sean
PMIP download script from Sean
Mengna GLSDBv2 status 3 level status transformation

***
