#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 29 10:03:02 2018

@author: akamolphat
"""
# =============================================================================
# Import prerequisite modules
# =============================================================================
from __future__ import division
import numpy as np
import calendar
from scipy import optimize

# =============================================================================
# Define prerequisite functions
# =============================================================================
def func(x, y):
    """This function is used to optimise for values x and y, based on the equation
    for MAT based on GDD0 and MTCM.
    
    This equation is only used when MTCM < 0 and GDD0 > 0
    
    If MTCM > 0, GDD0 must be > 0, T0 = GDD0/2*pi
    
    Units:
    GDD0 = K rad
    
    u = MAT/(MAT-MTCM)
    
    Prerequisite:
        numpy module (import numpy as np)
    
    Args:
        x = u
        y = GDD0/Tmin
    """
    # y has to be negative for it to find a root
    # y also cannot be equal to zero, as that would mean that GDD0 = 0 and it must be really cold (no pollen anyways)
    return(2*(x*np.arccos(-x) + np.sqrt(1 - x**2))/(1-x) + y)

def optimize_func(min_u, max_u, t0):
    """This function is used to optimise to calculate u
    
    Prerequisite:
        scipy.optimize (from scipy import optimize)
    
    Args:
        min_u: minimum value for u (MAT/(MAT-MTCM)), numeric, unitless
        max_u: maximum value for u (MAT/(MAT-MTCM)), numeric, unitless
        t0: value for GDD0/Tmin, numeric (radians)
    
    Returns:
        u: (MAT/MAT-MTCM)
    
    """
    return(optimize.brentq(func, a = min_u, b = max_u, args = (t0)))

def days_in_year_func(x):
    """This function returns the number of days in a year based on the year.
    
    It is used when converting GDD0 from K day to K rad
    
    Prerequisite:
        calendar module
    
    Args:
        x: year, numeric (CE)
    
    Returns:
        number of days in that year (365 or 366)
    
    """
    if calendar.isleap(x):
        return(366)
    else:
        return(365)

def MAT_from_GDD0_MTCM(GDD0_Krad, MTCM):
    """This function calculates mean annual temperature (MAT) from GDD0_Krad (Growing Degree Days 0C) 
    and MTCM (Mean Temperature of the Coldest Month).
    
    Prerequisite:
        numpy module (import numpy as np)
        optimize_func (defined above)
        func (defined above)
    
    Args:
        GDD0_Krad: GDD0, array (K rad)
        MTCM: MTCM, array (Celsius)
        
    Returns:
        An array of the same shape as GDD0_Krad and MTCM
        
    Raises:
        prints warning if the shape of the input GDD0_Krad and MTCM are not the same 
    
    Note that the input is an array and not just float/numeric
    
    """
    if np.shape(GDD0_Krad) != np.shape(MTCM):
        print('The dimensions/shapes of GDD0 and MTCM is different from each other')
        return(None)
    else:
        # create empty to store MAT
        MAT = np.empty(np.shape(GDD0_Krad))
        MAT[:] = np.nan
        # =============================================================================
        # If Tmin >= 0 MAT = GDD0_Krad/(2*pi)
        # =============================================================================
        MAT[MTCM >= 0] = GDD0_Krad[MTCM >= 0]/(2*np.pi)
        # =============================================================================
        # If Tmin < 0 and GDD0 = 0, MAT = less than Tmin/2 but cannot be accurately determined 
        # =============================================================================
        if len(MTCM[(MTCM < 0) & (GDD0_Krad == 0.0)]) > 0:
            print('There are values where MTCM < 0 and GDD0 = 0. Only the maximum MAT can be determined (Tmin/2). Return -9999 as values')
            MAT[(MTCM < 0) & (GDD0_Krad == 0.0)] = -9999
        # MAT[(MTCM < 0) & (GDD0_Krad == 0.0)] = MTCM[(MTCM < 0) & (GDD0_Krad == 0.0)]/2
        # =============================================================================
        # If Tmin >= 0 and GDD0 = 0, something is fishy
        # =============================================================================
        if len(MAT[(MTCM >= 0) & (GDD0_Krad == 0.0)]) > 0:
            print('There seems to be some values where Tmin >= 0 and GDD0 = 0; This is very fishy and should never happen (would mean that MTCM was not really MTCM)')
        # =============================================================================
        # If Tmin < 0 and GDD0 > 0, MAT can be calculated using the optimize method
        # =============================================================================
        t0 = GDD0_Krad/MTCM
        min_u = -1 # minimum valid value for 'u' is -1
        max_u = 0.9999999999 # maximum valid value for 'u' is 1
        
        t0_input = t0[(MTCM < 0) & (GDD0_Krad > 0)]
        u = np.array(map(optimize_func, np.repeat(min_u, len(t0_input)), np.repeat(max_u, len(t0_input)), t0_input))
        MAT[(MTCM < 0) & (GDD0_Krad > 0)] = -MTCM[(MTCM < 0) & (GDD0_Krad > 0)]*u/(1 - u)
        return(MAT)
