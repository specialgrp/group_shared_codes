## Python folder
This folder (python/) contains snippets of codes in python 

### calc_annual_from_months_standard.py
This script creates either annual mean or annual total netCDF file from monthly netCDF file. The script assumes a standard calendar, and allows looping through multiple files. If your calendar system is not standard, this will require alterations. The script can be executed from the command line as follows:

* python calc_annual_from_months_standard.py GPP 'input/GPP*.nc' output_file.nc 0 True

where GPP is the variable name, 'input/GPP*.nc' is the pattern to the input files (if this is only one file, this is the path to the file), output_file.nc is the path to the output file, 0 is the starting layer (1 if starting layer is December of previous year, 2 if this is November of previous year, etc.) and True indicates that this is annual mean (False for annual total).

Contact: **Kamolphat Atsawawaranunt (k.atsawawaranunt@reading.ac.uk)**

### convert2minus180to180.py
This script converts netCDF files which are in 0 to 360 degrees format to -180 to 180 degrees format. It can be executed from the command line as follows:

 * python convert2minus180to180.py input.nc longitude output.nc
 
 where input.nc is the path to the input netCDF file, longitude is the name of the longitude variable in the netCDF file (in some cases, this is called 'lon' instead) and output.nc is the path to the output netCDF file.

Contact: **Kamolphat Atsawawaranunt (k.atsawawaranunt@reading.ac.uk)**

### extract_netCDF_shapefiles.py
This script crops a netCDF file based on a supplied shapefile and returns a netCDF file with the values outside of the shapefile converted to NA. Script only tested with 2 and 3 dimensional netCDF file but should work with up to variable with 5 dimensions. This script can be executed from the command line as follows:

 * python extract_netCDF_shapefiles.py input.nc varname input.shp output.nc False
 
where input.nc is the path to the input netCDF file, varname is the name of the variable of interest, input.shp is the path to the shape file, output.nc is the path to the output netCDF file and False refers to whether or not the output netCDF file is returned with the same coverage as the input netCDF file. If True, all cells outside of the shapefile converted to NA, but the output netCDF file will still cover the same area as the input file (i.e. global). If False (default when not supplied), all the cells outside of the shapefile is converted to NA, but the output netCDF file will only cover the minimum area required to contain cells with a valid value, making the output file much smaller. 

Contact: **Kamolphat Atsawawaranunt (k.atsawawaranunt@reading.ac.uk)**

### extractvar2points.py
This script extract values from 3 dimensional netCDF file based on a supplied latitude and longitude in a csv file. This script can be executed from the command line as follows:

 * python extractvar2points.py input.nc varname input.csv output.csv
 
where input.nc is the path to the input netCDF file, varname is the name of the variable of interest, input.csv is the path to the csv file with coordinates and output.csv is the path to the output csv file.

Contact: **Kamolphat Atsawawaranunt (k.atsawawaranunt@reading.ac.uk)**

### extract_timeseries.py
This script extracts area-weighted mean time series from netCDF file (base on the latitudes) and returns a csv and pdf file. This ignores NA in the netCDF file. This script can be executed from the command line as follows:

 * python extract_timeseries input.nc varname output.csv input.csv
 
 where input.nc is the path to the input netCDF file, varname is the name of the variable in the netCDF file, output.csv is the path to the output CSV file, output.pdf is the path to the output PDF file.

Contact: **Kamolphat Atsawawaranunt (k.atsawawaranunt@reading.ac.uk)**

### MAT_from_GDD0_MTCM_function.py
This script contains the function for calculating MAT from GDD0 and MTCM. There is a function called days_in_year which is used to help convert GDD0 from K day to K rad (if including leap years). The fundamentals of the equation was thought of by Professor Iain Colin Prentice.

Contact: **Kamolphat Atsawawaranunt (k.atsawawaranunt@reading.ac.uk)**

### monthly_clim_calc.py
This script creates a monthly climatology netCDF file (based on a time period) from monthly netCDF file. This script can be executed from the command line as follows:

 * python input.nc output.nc varname 1970 2000
 
 where input.nc is the path to the input netCDF file, output.nc is the path to the output netCDF file, varname is the variable name in input.nc, 1970 is the start year of interest, 2000 is the end year of interest.

Contact: **Kamolphat Atsawawaranunt (k.atsawawaranunt@reading.ac.uk)**

### interpolate_autoregression_conserve_mean.py
This script contains a function which performs an autoregressive interpolation while conserving the mean of the time period. The function also allows for an option for minimum and maximum bounds as well which the spline option does not offer. The algorithm was taken from Rymes and Myers (2001), but a few equations were altered by Kamolphat Atsawawaranunt in order to get the algorithm to work. If use, please cite:

    Rymes, M.D. and Myers, D.R., 2001. Mean preserving algorithm for smoothly interpolating averaged data. Solar Energy, 71(4), pp.225-231. doi: 10.1016/S0038-092X(01)00052-4

and please contact **Kamolphat Atsawawaranunt (k.atsawawaranunt@reading.ac.uk)**

### interpolate_spline_conserve_mean.py
This script contains a function which performs spline interpolation while conserving the mean of the time period. The function was originally designed for interpolation from monthly to daily data, but can also be applied to other types of data (i.e. weekly to daily, or subdaily to hourly). **Please note that the current function do not have limits on the interpolation curve; i.e. in cases of precipitation, the minimum value cannot be less than 0. This would require some modification.**

Contact: **Kamolphat Atsawawaranunt (k.atsawawaranunt@reading.ac.uk)**

### interpolate_netCDF.py
This script contains a function which interpolates/regrid netCDF files. The function has the following arguments:  

 * Required inputs:  	
	* input_netcdf  	= string (path to netcdf input file)  
    * output_netcdf 	= string (path to output netcdf file name)  
	* method 		 	= string (method of interpolation):  
		* "Nearest" = Nearest Neighbour  
		* "Linear" = Linear interpolation  
		* "AreaWeighted" = Area Weighted  
	* new_lat_dim 	= numeric (output latitude cell size)  
	* new_lon_dim 	= numeric (output longitude cell size)  
* Optional inputs:  
    * new_lat_start 	= numeric (latitude boundary (lower bound))  
    * new_lat_stop  	= numeric (latitude boundary (upper bound))  
    * new_lon_start 	= numeric (longitude boundary (lower bound))  
    * new_lon_stop  	= numeric (longitude boundary (upper bound))  
    * author_details 	= string (details of person who executed this function, to be stored in netcdf file attributes)  
    * mask 				= string (path to masked netCDF file (netcdf file with lat and lon dimension, NO TIME dimension))  
    * mask_values 		= values to be masked off (i.e. if sea=0, and land=1, masked_values = 0 if land variables wanted)  
    * output_netcdf_masked = string (path to output interpolated + masked netcdf file name)  

The function is called as follows:  

 1. from interpolate_netCDF import int_netcdf # the script must be in the same folder as where this is being called, or in the python path  
 2. run_int_netcdf = int_netcdf()  
 3. run_int_netcdf.interpolate_netcdf_iris(......) # ...... are the arguments for the interpolate_netcdf_iris function  

Contact: **Kamolphat Atsawawaranunt (k.atsawawaranunt@reading.ac.uk)**

### spatialglm_timeperiod.py
This script performs GLM on every grid cell over a defined time period. However, it requires a very specific input and the user will have to edit the file to suit their needs. It is only provided here so that the user will not have to reinvent the wheel from scratch. Go through the script manually if the user wants to use this.

Contact: **Kamolphat Atsawawaranunt (k.atsawawaranunt@reading.ac.uk)**

### MI_adjuster.py
This script corrects reconsturctions of moisture index (MI) for changes in atmospheric [CO_2]. The user must supply the:

 * original reconstructed MI
 * modern temperature
 * change in temperature between the modern and palaeo time period
 * ratio between palaeo and modern [CO_2]

Corrections can be made through the `calculate_m_true` function. The method for this process is outlined in:

    Prentice, I.C., Cleator, S.F., Huang, Y.H., Harrison, S.P., Roulstone, I., 2017. Reconstructing ice-age palaeoclimates: Quantifying low-CO2 effects on plants. Global and Planetary Change 149, 166-176.

Contact: **Sean F. Cleator (s.cleator@surrey.ac.uk)**
