#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jul  5 11:19:00 2017

@author: Kamolphat Atsawawaranunt

1. perform GLM on each cell and output a netCDF file

# This script only works with very specific input
# It is provided to the group so that if someone is perform GLMs on netCDF files again, they would not have to reinvent the wheel.
# In order for the script to work, edits will have to be done to the scripts. Check #EDITS?


"""
# Import prerequisite functions
from glob import glob
import netCDF4 as nc4
import os, sys
import numpy as np
import pandas as pd
import statsmodels.api as sm

# define functions to scale the variables and standardise the variable
def scale(input_var):
    """scale/standardise variables based on the mean and the standard deviation
    name: scale
    input: numeric/array
    output: scaled values"""
    input_mean = np.mean(input_var)
    input_sd = np.std(input_var)
    return((input_var - input_mean)/input_sd)



#==============================================================================
# Perform glm on each cell
# 1. Read in the variables
# 2. Perform glm
# Variables
# tas
# clt
# CO2
# VPD, rerun and output this
# PPFD, rerun and output this
#==============================================================================

path2folder = sys.argv[1]
outputfile = sys.argv[2]
start_year = int(sys.argv[3])
end_year = int(sys.argv[4])
if len(sys.argv) == 5:
    print('Only 4 arguments supplied, the default would be used; standardised = False and logtransform link = False')
    standardised = False
    logtransform = False
elif len(sys.argv) == 6:
    standardised = sys.argv[5]
    print('Only 5 arguments supplied, the default would be used; logtransform link = False')
    logtransform = False
    if standardised in ['True', 'true', 't', 'T']:
        standardised = True
    elif standardised in ['False', 'false', 'f', 'F']:
        standardised = False
    else:
        print('stardised argument not correct; Either "True" or "False" in 5th argument')
        exit
elif len(sys.argv) == 7:
    logtransform = sys.argv[6]
    if standardised in ['True', 'true', 't', 'T']:
        standardised = True
    elif standardised in ['False', 'false', 'f', 'F']:
        standardised = False
    else:
        print('stardised argument not correct; Either "True" or "False" in 5th argument')
        exit
    if logtransform in ['True', 'true', 't', 'T']:
        logtransform = True
    elif logtransform in ['False', 'false', 'f', 'F']:
        logtransform = False
    else:
        print('logtransform link argument not correct; Either "True" or "False" in 5th argument')
        exit

# Defining input netCDF based on regular expression #EDITS?
tas_file = glob(os.path.join(path2folder, 'tas_*.nc'))[0] #EDITS?
GPP_file = glob(os.path.join(path2folder, 'GPP_*SWdown.nc'))[0] #EDITS?
vpd_file = glob(os.path.join(path2folder, 'vpd_*.nc'))[0] #EDITS?
PPFD_file = glob(os.path.join(path2folder, 'PPFD_*SWdown.nc'))[0] #EDITS?
co2_file = glob(os.path.join(path2folder, 'co2_*.csv'))[0] #EDITS?

f = nc4.Dataset(tas_file, 'r')
lons = f.variables['longitude'][:]
lats = f.variables['latitude'][:]
time = f.variables['time'][:]
time_idx = np.flatnonzero((time >= start_year) & (time <= end_year))
annual_tas = f.variables['%s' %'tas'][time_idx,:,:]
f.close()
try:
    f = nc4.Dataset(GPP_file, 'r')
    annual_GPP = f.variables['%s' %'GPP'][time_idx,:,:]
    f.close()
except:
    'something is wrong with GPP_file'

try:
    f = nc4.Dataset(vpd_file, 'r')
    annual_vpd = f.variables['%s' %'vpd'][time_idx,:,:]
    f.close()
except:
    'something is wrong with tas_file'
    
try:
    f = nc4.Dataset(PPFD_file, 'r')
    annual_PPFD = f.variables['%s' %'PPFD'][time_idx,:,:]
    f.close()
except:
    'something is wrong with PPFD_file'

# Add more variables here if this is the case #EDITS?

co2_data = np.loadtxt(open(co2_file, "r"), delimiter=",", skiprows=28) #EDITS?

co2_data = co2_data[np.where((co2_data[:,0] >= start_year) & (co2_data[:,0] <= end_year)), 1] # index based on dates #EDITS?

# Alter the output to accomodate for the data you would be storing #EDITS?
var_fill =  9.96921e36
output_file = nc4.Dataset(outputfile, 'w')
output_file.createDimension('latitude', len(lats))
output_file.createDimension('longitude', len(lons))
output_file.createDimension('values', 2)
latitudes = output_file.createVariable('latitude', np.float32, ('latitude',))
longitudes = output_file.createVariable('longitude', np.float32, ('longitude',))
values = output_file.createVariable('values', int, ('values',))
tas = output_file.createVariable('%s' %'tas_slope', np.float, ('values', 'latitude', 'longitude'), fill_value=var_fill)
tas.units = 'unitless'
PPFD = output_file.createVariable('%s' %'PPFD_slope', np.float, ('values', 'latitude', 'longitude'), fill_value=var_fill)
PPFD.units = 'unitless'
vpd = output_file.createVariable('%s' %'vpd_slope', np.float, ('values', 'latitude', 'longitude'), fill_value=var_fill)
vpd.units = 'unitless'
co2 = output_file.createVariable('%s' %'co2_slope', np.float, ('values', 'latitude', 'longitude'), fill_value=var_fill)
co2.units = 'unitless'
intercept = output_file.createVariable('%s' %'intercept', np.float, ('values', 'latitude', 'longitude'), fill_value=var_fill)
intercept.units = 'unitless'
latitudes.units = 'degrees north'
longitudes.units = 'degrees east'
values.units = '0=coef, 1=p_values'
latitudes[:] = lats
longitudes[:] = lons
values[:] = [0, 1]
index = np.where(np.isfinite(annual_GPP))
#lat_lon_index = np.array(np.where(np.isfinite(annual_GPP[0,:,:])))
lat_lon_index = np.array(np.where((np.nanmean(annual_GPP, axis = 0) > 0) & (np.isfinite(np.nanmean(annual_tas, axis = 0)))))
#lat_lon_index = np.array(np.where(np.isfinite(np.nanmean(annual_tas, axis = 0)))) # when summing the values, annual GPP becomes 0 instead of NA, so use tas as mask #EDITS?

# Alter the variable names to match your needs #EDITS?
for i in range(0,lat_lon_index.shape[1]):
    idx = lat_lon_index[:,i]
    lat_idx = idx[0]
    lon_idx = idx[1]
    if standardised == True:
        d = {'annual_GPP': scale(annual_GPP[:, lat_idx, lon_idx]), 
        'annual_tas': scale(annual_tas[:,lat_idx, lon_idx]),
        'annual_PPFD': scale(annual_PPFD[:,lat_idx, lon_idx]),
        'annual_vpd': scale(annual_vpd[:,lat_idx, lon_idx]),
        'annual_co2': scale(co2_data[0])
        }
    else:
        d = {'annual_GPP': annual_GPP[:, lat_idx, lon_idx], 
        'annual_tas': annual_tas[:,lat_idx, lon_idx],
        'annual_PPFD': annual_PPFD[:,lat_idx, lon_idx],
        'annual_vpd': annual_vpd[:,lat_idx, lon_idx],
        'annual_co2': co2_data[0]
        }
    tb = pd.DataFrame(data = d)
    #tb = tb[tb.annual_GPP > 0] #NOT SURE I SHOULD COMMENT THIS OUT
    tb = tb[np.isfinite(tb.annual_GPP)]
    tb = tb[np.isfinite(tb.annual_tas)]
    #model = sm.ols(formula='annual_GPP ~ annual_clt', dt = tb)
    if tb.shape[0] != 0:
	if logtransform == False:
            model = sm.formula.glm(formula='annual_GPP ~ annual_tas + annual_PPFD + annual_vpd + annual_co2', family = sm.families.Gaussian(), data = tb)
        elif logtransform == True:
            model = sm.formula.glm(formula='annual_GPP ~ annual_tas + annual_PPFD + annual_vpd + annual_co2', family = sm.families.Gaussian(link = sm.genmod.families.links.log), data = tb)
        fitted = model.fit()
        tas[0, lat_idx, lon_idx] = fitted.params['annual_tas']
        PPFD[0, lat_idx, lon_idx] = fitted.params['annual_PPFD']
        vpd[0, lat_idx, lon_idx] = fitted.params['annual_vpd']
        co2[0, lat_idx, lon_idx] = fitted.params['annual_co2']
        intercept[0, lat_idx, lon_idx] = fitted.params['Intercept']
        tas[1, lat_idx, lon_idx] = fitted.pvalues['annual_tas']
        PPFD[1, lat_idx, lon_idx] = fitted.pvalues['annual_PPFD']
        vpd[1, lat_idx, lon_idx] = fitted.pvalues['annual_vpd']
        co2[1, lat_idx, lon_idx] = fitted.pvalues['annual_co2']
        intercept[1, lat_idx, lon_idx] = fitted.pvalues['Intercept']
    else:
        print('lat lon index %d sum to 0 or nan, no variation and therefore cannot perform glm' %i)
        # NEEDS TO FIND A WAY TO REDUCE THESE CELLS WITH EMPTY 

output_file.close()

