# -*- coding: utf-8 -*-
"""
Created on Wed May 18 12:55:38 2017

@author: Kamolphat Atsawawaranunt

This script read daily csv files of climate varibales and elevation and run splash
The columns in the elevation csv file must be as follows:
    Lat, Long, Elv, key

The columns in the climate variables csv file must be as follows:
    Lat, Long, key, day1, day2, etc.......

The days must be in order from left to right
The number of columns (excluding Lat, Long and key) in the climate variables csv file must also be greater 
than the number of days you are running.

The script follows the following logic:
    1. Read in the climate variables and elevation csv 
    2. Specify start and end date for SPLASH runs
    3. Sort and convert from dataframe to matrix
    4. Loop through each day
        i. if first day, run spin up to get soil moisture content
    5. Loop through each row of datamatrix
        
"""
#==============================================================================
# IMPORT MODULES and SCRIPTS 
#==============================================================================
import sys
import logging
import numpy as np
import pandas as pd
from splash import SPLASH
from dateutil.relativedelta import relativedelta
from datetime import timedelta, date

#==============================================================================
# Variables to amend
#
# This can be edited manually or input from the command line as follows:
#
# python splash_wrapper_A.py elev.csv pre.csv tmp.csv, sf.csv 2001 2001 aet_output.csv eet_output.csv pet_output.csv wn_output.csv
#
#==============================================================================

# INPUT FILES

elev_file_input = sys.argv[1] #'p_elev.csv' # elevation file
d_prec_file_input = sys.argv[2] #'p_test.csv'  # precipitation file
d_temp_file_input = sys.argv[3] #'t_test.csv' # temperature file
d_sunf_file_input = sys.argv[4] #'s_test.csv' # fractional sunshine file

# TIME PERIOD
start_year = int(sys.argv[5]) #2001
end_year = int(sys.argv[6]) #2001

# OUTPUT FILES
aet_file_output = sys.argv[7] #'aet_output_wlog.csv'
eet_file_output = sys.argv[8] #'eet_output_wlog.csv'
pet_file_output = sys.argv[9] #'pet_output_wlog.csv'
wn_file_output = sys.argv[10] #'wn_output_wlog.csv'

# Name of the key column
key_name = 'key'

#==============================================================================
# Define functions which will be used in this script
#==============================================================================

#==============================================================================
# Function name: daterange_daily 
# Require: start date as date object (start_date)
#          end date as date object (end_date)
# Return range of dates from start date to end date
#==============================================================================
def daterange_daily(start_date, end_date):    
    for n in range(int ((end_date - start_date).days)):
        yield start_date + timedelta(n)

#==============================================================================
# Read in climate data and elevation data using pandas
#==============================================================================
elev = pd.read_table(elev_file_input, sep=',')
d_pre = pd.read_table(d_prec_file_input, sep=',') 
d_tmp = pd.read_csv(d_temp_file_input, sep=',')
d_sun = pd.read_csv(d_sunf_file_input, sep=',')

#==============================================================================
# Specify start and end date for SPLASH to run 
#==============================================================================
start_date = date(start_year, 1, 01)
end_date = date(end_year, 12, 31)

# disable logging
logging.disabled = True
#==============================================================================
# Create a list of keys
#==============================================================================
my_key = list(set(elev.loc[:, key_name])) 
#==============================================================================
# If we are going by the key column, we have to make sure that there are no
# duplicated keys.
# We only need to check for duplicated keys in the elevation table as that
# is what we go by.
#==============================================================================
if len(my_key) != len(elev.loc[:, key_name].index):
    sys.exit('There are duplicated keys in the elevation table')
#==============================================================================
# Subset other tables based on the keys
#==============================================================================
d_sun = d_sun.loc[d_sun[key_name].isin(my_key),:]
d_pre = d_pre.loc[d_pre[key_name].isin(my_key),:]
d_tmp = d_tmp.loc[d_tmp[key_name].isin(my_key),:]
if len(d_sun.index) != len(my_key):
    sys.exit('The number of rows with keys in sunshine table is not the same as that of the elevation table, once the keys are matched')
if len(d_pre.index) != len(my_key):
    sys.exit('The number of rows with keys in precipitation table is not the same as that of the elevation table, once the keys are matched')
if len(d_tmp.index) != len(my_key):
    sys.exit('The number of rows with keys in temperature table is not the same as that of the elevation table, once the keys are matched')
#==============================================================================
# Sort the tables based on keys
#
# We want the keys to be in order as we are going to convert these to a matrix
# as it is much faster to subset through a matrix than subset using keys
#
#==============================================================================
elev = elev.sort_values(by = [key_name])
d_sun = d_sun.sort_values(by = [key_name])
d_pre = d_pre.sort_values(by = [key_name])
d_tmp = d_tmp.sort_values(by = [key_name])
#==============================================================================
# Subset everything other that the lat, long and keys
# and convert to matrix
# In this case, the values starts at column 4 (python index = 3)
#==============================================================================
elev_val = elev['Elv'].values
lat_val = elev['Lat'].values
d_sun = d_sun.iloc[:,3:].values
d_pre = d_pre.iloc[:,3:].values
d_tmp = d_tmp.iloc[:,3:].values
#==============================================================================
# The total number of days in the run
#==============================================================================
day_no = (end_date - start_date).days + 1
#==============================================================================
# Check that there is at least more data than the number of days to run
#==============================================================================
if d_sun.shape[1] < day_no:
    sys.exit('There are less daily data in sunshine table than there are number of days')
if d_pre.shape[1] < day_no:
    sys.exit('There are less daily data in precipitation table than there are number of days')
if d_tmp.shape[1] < day_no:
    sys.exit('There are less daily data in temperature table than there are number of days')
#==============================================================================
# Create a matrix to store the output
#==============================================================================
aet_mat = np.empty((len(my_key),(day_no)))
aet_mat[:] = np.nan
eet_mat = aet_mat.copy()
wn_mat = aet_mat.copy()
pet_mat = aet_mat.copy()

#==============================================================================
# Loop through each day
#==============================================================================
date_ls = []
for my_date in daterange_daily(start_date, end_date + relativedelta(days = 1)):  
    doy = my_date.timetuple().tm_yday  #day of the year
    doy_idx = doy-1
    yr = my_date.year
    yesterday = my_date - timedelta(1)
    # convert my_date to string
    my_date_str = my_date.strftime('%Y_%m_%d')
    date_ls.append(my_date_str)
    # print out the date it is currently running
    print(my_date_str)
    yesterday_str = yesterday.strftime('%Y_%m_%d')
    if my_date == start_date:
        # Loop through each row in the matrix
        for i in range(0,len(my_key)):
            # Initiate SPLASH
            run_splash = SPLASH(lat_val[i], elev_val[i])
            # Run spin up until equilibrium, using the current year's data but 
            # with previous year climate
            run_splash.spin_up_A(start_date.year - 1, d_pre[i, :], 
                                                       d_sun[i, :], 
                                                       d_tmp[i, :])
            # RUN SPLASH
            run_splash.run_one_day(doy, yr, run_splash.wn, 
                                            d_sun[i, 0], 
                                            d_tmp[i, 0], 
                                            d_pre[i, 0])
            # Store the output
            aet_mat[i, 0] = run_splash.aet
            eet_mat[i, 0] = run_splash.eet
            wn_mat[i, 0] = run_splash.wn
            pet_mat[i, 0] = run_splash.pet
    else:
        # Loop through each row in the matrix
        for i in range(0,len(my_key)):
            # Initiate SPLASH
            run_splash = SPLASH(lat_val[i], elev_val[i])
            # RUN SPLASH
            run_splash.run_one_day(doy, yr, wn_mat[i,(doy_idx - 1)], 
                                            d_sun[i,doy_idx], 
                                            d_tmp[i,doy_idx], 
                                            d_pre[i,doy_idx])
            # Store the output
            aet_mat[i, doy_idx] = run_splash.aet
            eet_mat[i, doy_idx] = run_splash.eet
            wn_mat[i, doy_idx] = run_splash.wn
            pet_mat[i, doy_idx] = run_splash.pet
#==============================================================================
# Convert matrices to dataframe
#==============================================================================
aet_df = pd.DataFrame(aet_mat, columns = date_ls)
aet_df = pd.concat([elev.reset_index(), aet_df], axis=1)
eet_df = pd.DataFrame(eet_mat, columns = date_ls)
eet_df = pd.concat([elev.reset_index(), eet_df], axis=1)
wn_df = pd.DataFrame(wn_mat, columns = date_ls)
wn_df = pd.concat([elev.reset_index(), wn_df], axis=1)
pet_df = pd.DataFrame(pet_mat, columns = date_ls)
pet_df = pd.concat([elev.reset_index(), pet_df], axis=1)
#==============================================================================
#  Save output (aet, eet, wn, and pet) dataframes to csv
#==============================================================================
aet_df.to_csv(aet_file_output, sep=',', index = False)
eet_df.to_csv(eet_file_output,sep=',', index = False)
wn_df.to_csv(wn_file_output,sep=',', index = False)
pet_df.to_csv(pet_file_output,sep=',', index = False)

    
