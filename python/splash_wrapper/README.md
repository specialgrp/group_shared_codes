# Documentation of A’s code to run SPLASH on daily csv format
**Updated 6th November 2018. Please read the splash licensing at the bottom before use**
## Main script
The main script is called “splash_wrapper_A.py”.
The codes follow the following logic:

1. Read in the climate variables and elevation csv 
2. Specify start and end date for SPLASH runs
3. Sort and convert from dataframe to matrix
4. Loop through each day
    a. if first day, run spin up to get soil moisture content
5. Loop through each row of datamatrix
6. output to csv file

## How to run the script

1. Open the terminal/command prompt and cd to the directory containing all the scripts and the input files. Run the following:

> *python splash_wrapper_A.py elevation_file.csv precipitation_file.csv temperature_file.csv fractionalsunshine_file.csv 2001 2001 aet_output.csv eet_output.csv pet_output.csv wn_output.csv*

* elevation_file.csv – path to elevation csv file with the following columns:
    - Lat, Long, Elv, key
* precipitation_file.csv – path to precipitation csv file with the following columns:
    - Lat, Long, key, day1, day2, etc.......
* temperature_file.csv – path to temperature csv file with the following columns:
    - Lat, Long, key, day1, day2, etc.......
* fractionalsunshine_file.csv – path to fractional sunshine csv file with the following columns:
    - Lat, Long, key, day1, day2, etc.......
* 2001 – start year
* 2001 – end year
* aet_output.csv – daily actual evapotranspiration output file
* eet_output.csv – daily equilibrium evapotranspiration output file
* pet_output.csv – daily potential evapotranspiration output file
* wn_output.csv – daily soil moisture content output file (max = 150mm)

## Notes:

* Soil moisture content maxes out at 150mm
* The equilibrium threshold for the spin up is set at 1mm, this can be edited in the spin_up_A function in splash.py

## Edits performed on splash.py:

* Please note that this edit is performed on as plash version retrieved from the SPLASH repository from bitbucket (https://bitbucket.org/labprentice/splash) on the 16th of March 2018.
* The function spin_up_A was added to splash.py. This function is identical to the spin_up function originally in splash, but take arrays as inputs instead of taking the ‘DATA’ class format (which A is having trouble to improvise). The function spin_up_A can be found in “spin_up_A.txt”.
---
# SPLASH license as cloned from SPLASH

SPLASH v.1.1-dev
================

License
=======

Copyright (C) 2016 Prentice Lab


Requirements
============

This code was successfully compiled and executed using Python 2.7 and Python 3.5
interpreters (Python Software Foundation, 2016).

This code requires the installation of third-party packages: NumPy (v.1.8.2,
v.1.10.4 by NumPy Developers, 2013) and SciPy (v.0.13.3, v.0.17.0 by SciPy
Developers, 2016) and references the basic date and time types (datetime),
logging facility (logging), Unix-style pathname pattern extension (glob), and
miscellaneous operating system interfaces (os) modules.

To run (Python 2/3):

`python main.py`
Results are printed to screen.
Requires input file (../../../data/example_data.csv).


To run tests (Python 2/3):

TEST 1:
`python solar.py`
Results are printed to file (solar.log)

TEST 2:
`python evap.py`
Results are printed to file (evap.log)

TEST 3:
`python splash.py`
Results are printed to file (splash.log) and screen.

To create input data (Python 2/3):

`python splash_data.py`
Results are printed to file (./out/splash_data_out.csv); make certain directory "out" exists before running.

Contents
========

const.py
    Contains definitions for SPLASH global constants.
    Libraries:
        - numpy

data.py
    DATA class definition for reading input data.
    Libraries:
        - logging
        - numpy

evap.py
    EVAP class definition for calculating daily evapotranspiration quantities.
    Libraries:
        - logging
        - numpy

main.py
    Main function for running the SPLASH code.
    Libraries:
        - logging

solar.py
    SOLAR class definition for calculating daily radiation fluxes.
    Libraries:
        - logging
        - numpy

splash.py
    SPLASH class definition for updating daily quantities of radiation, evapotranspiration, soil moisture and runoff.
    Libraries:
        - logging
        - numpy

splash_data.py
    SPLASH_DATA class and script to produce a CSV file with daily input data (i.e., sunshine fraction, air temperature, and precipitation)
    Libraries:
        - datetime
        - glob
        - logging
        - numpy
        - os.path
        - scipy.io.netcdf

utilities.py
    Contains utility functions that are shared amongst classes.
    Libraries:
        - logging
        - numpy
