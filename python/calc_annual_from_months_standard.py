#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jul  5 14:07:46 2017

@author: Kamolphat Atsawawaranunt

calc_annual_from_months_weighted does not index layers based on time

Note that this script is monthly weighted when calculating annual values (i.e. 31 days month will be worth more than 30 or 29/28 days month in calculating the mean)

Note that this script assumes that all input has the same area of valid values. If there are 'NA' within the twelve months used to calculate the annual mean or sum, this will result in NaN

If user has input which does not have a standard calendar, the user will have to alter this code, mainly the weights.

As there are quite a handful of calendars, I have not altered to codes to allow these options (mainly because I have trouble distinguishing julian, proleptic gregorian and gregorian calendar)

This script allows looping through multiple files as it was originally designed for the CMIP5 outputs.

Please always check the output units and the long names

"""

# Import prerequisite modules
import sys, os
import numpy as np
from glob import glob
import netCDF4 as nc4
import calendar

# Input variables from command line/ terminal
varName = sys.argv[1]
var_file_pattern = sys.argv[2]
output_file_name = sys.argv[3]
print(len(sys.argv))
print(sys.argv)
if len(sys.argv) < 4:
    print('At least 3 arguments required; only ' + str(len(sys.argv) - 1) + ' were supplied')
    exit
else:
    pass
if len(sys.argv) <= 6:
    if len(sys.argv) == 4:
        start_idx = 0
        mean = True
    elif len(sys.argv) == 5:
        start_idx = int(sys.argv[4])
        mean = True
    elif len(sys.argv) == 6:
        start_idx = int(sys.argv[4])
        mean = sys.argv[5]
        if mean in ['True', 'true', 't', 'T']:
            mean = True
        elif mean in ['False', 'false', 'f', 'F']:
            mean = False
        else:
            print('mean argument not correct; Either "True" or "False" in 5th argument')
            exit
    print('varName = %s' %varName)
    print('var_file_pattern = %s' %var_file_pattern)
    print('output_file_name = %s' %output_file_name)
    print('start_idx = %i' %start_idx)
    print('mean = %s' %str(mean))
    
# Define function
def calc_annual(varName, var_file_pattern, output_file_name, start_idx = 0, mean = True):
    """"Calculate either annual mean or annual total based on monthly data

    name: calc_annual

    input:  varName (string: name of variable, i.e. 'GPP')
            var_file_pattern (string: file pattern, i.e. 'inputfolder/GPP*.nc'
            output_file_name (string: output file name, i.e. 'output.nc')
	    start_idx (integer: starting layer, i.e. 1 if the first layer of first file refers to december month of previous year) (default = 0)
            mean (boolean: default = True, if False, Sum is calculated instead (i.e. GPP))

    output: netCDF file written to output_file_name

    """
    # Create a list of files to loop through
    var_file_pattern = var_file_pattern.replace('"', '')
    var_file_pattern = var_file_pattern.replace("'", "")
    tas_ls = glob(var_file_pattern)
    print(tas_ls)
    tas_ls.sort()
    # Open the first netCDF file to get the attributes, especially the latitude and longitudes
    f = nc4.Dataset(tas_ls[0], 'r')
    try:
        lons = f.variables['longitude'][:]
    except:
        print("some error, longitude variable not called 'longitude', trying 'lon';")
        lons = f.variables['lon'][:]
    try:
        lats = f.variables['latitude'][:]
    except:
        print("some error, latitude variable not called 'latitude', trying 'lat';")
        lats = f.variables['lat'][:]
    time = f.variables['time'][:]
    time_units = f.variables['time'].units
    time_calendar = f.variables['time'].calendar
    var = f.variables['%s' %varName][:]
    print(np.max(var), np.min(var))
    var_fill = 9.96921e36
    var_units = f.variables['%s' %varName].units
    f.close()
    # Create new netCDF object to store yearly data
    output_file = nc4.Dataset(output_file_name, 'w')
    # Define dimensions
    output_file.createDimension('latitude', len(lats))
    output_file.createDimension('longitude', len(lons))
    output_file.createDimension('time', None)
    # Create variables
    time_outpt = output_file.createVariable('time', np.float, ('time',))
    latitudes = output_file.createVariable('latitude', np.float32, ('latitude',))
    longitudes = output_file.createVariable('longitude', np.float32, ('longitude',))
    main = output_file.createVariable('%s' %varName, np.float, ('time', 'latitude', 'longitude'), fill_value=var_fill)
    # Define units
    latitudes.units = 'degrees_north'
    longitudes.units = 'degrees_east'
    if mean:
        main.units = var_units
    else:
        var_units = var_units.replace('month', 'year')
        main.units = var_units
    main.units = var_units
    main._missing_value = var_fill
    # assign latitudes and longitudes from the first file
    latitudes[:] = lats
    longitudes[:] = lons
    time_series = []
    ctr = 0
    file_ctr = 0
    # Loop through each file
    for i in tas_ls:
        file_ctr += 1
        # read netcdf
        f = nc4.Dataset(i, 'r')
	try:
	    lons = f.variables['longitude'][:]
	except:
	    print("some error, longitude variable not called 'longitude', trying 'lon';")
	    lons = f.variables['lon'][:]
	try:
	    lats = f.variables['latitude'][:]
	except:
	    print("some error, latitude variable not called 'latitude', trying 'lat';")
            lats = f.variables['lat'][:]
        time = f.variables['time'][:]
        time_units = f.variables['time'].units
        time_calendar = f.variables['time'].calendar
        var = f.variables['%s' %varName][:]
        var_units = f.variables['%s' %varName].units
        if file_ctr == 1:
            start_idx = start_idx
            #firstyear = nc4.num2date(f.variables['time'][start_idx], time_units, time_calendar).year
        else:
            start_idx = 0
        # extract baseline date. baseline date in this case is the year of the first layer
        baseline_date = f.variables['time'][start_idx]
        baseline_yr = nc4.num2date(baseline_date, time_units, time_calendar).year
        time_len = len(time)
        for j in np.arange(start_idx, time_len, 12):
            if mean:
		if calendar.isleap(baseline_yr):
		    weights = np.array([31,29,31, 30, 31, 30, 31, 31, 30, 31, 30., 31.])
		else:
		    weights = np.array([31,28,31, 30, 31, 30, 31, 31, 30, 31, 30., 31.])
		Annual_var = np.sum(var[range(j,j+12)]*weights[:,np.newaxis,np.newaxis]/np.sum(weights), axis = 0)   
            else:
		if calendar.isleap(baseline_yr):
		    weights = np.array([31,29,31, 30, 31, 30, 31, 31, 30, 31, 30., 31.])
		else:
		    weights = np.array([31,28,31, 30, 31, 30, 31, 31, 30, 31, 30., 31.])
		Annual_var = np.sum(var[range(j,j+12)]*weights[:,np.newaxis,np.newaxis]/np.sum(weights), axis = 0)*12.
            main[ctr,:,:] = Annual_var
            ctr += 1
            time_series = np.append(time_series, baseline_yr)
            baseline_yr = baseline_yr + 1
        f.close()
    time_outpt.units = "years since 0000"
    time_outpt[:] = np.array(time_series)
    output_file.close()

calc_annual(varName = varName,
    var_file_pattern = var_file_pattern,
    output_file_name = output_file_name,
    start_idx = start_idx,
    mean = mean)

