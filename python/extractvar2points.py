#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 28 10:44:43 2017

@author: pe915155
"""

import pandas as pd
import numpy as np
import netCDF4 as nc4
import re
import sys

inputnetcdf = sys.argv[1]#''WFDEI/Tair_WFDEI_200101.nc'
inputVar = sys.argv[2]# 'Tair'
inputcsv = sys.argv[3]#'WFDEI/flux_sites.csv'
outputcsv = sys.argv[4]#'WFDEI/output_sites.csv'
try:
    tb_results = pd.read_csv(inputcsv)
except:
    raise
    sys.exit('Cannot read input csv file. Check if this is supplied correctly')

try:
    f = nc4.Dataset(inputnetcdf, 'r')
except:
    raise
    sys.exit('Cannot open netCDF file.')

try:
    lon = f.variables['lon'][:]
except:
    try:
    	lon = f.variables['longitude'][:]
    except:
        raise
        sys.exit('Cannot read in the longitude variable, tried both "lon" and "longitude".')
try:
    lat = f.variables['lat'][:]
except:
    try:
	lat = f.variables['latitude'][:]
    except:
	raise
        sys.exit('Cannot read in the latitude variable, tried both "lat" and "latitude".')
try:
    time = f.variables['time'][:]
except:
    raise
    sys.exit('Cannot read in the time variable. Check if netCDF file has variable called "time".')
try:
    time_units = f.variables['time'].units
except:
    raise
    sys.exit('Cannot read in the time units. Check if time variable has "units".')
try:
    time_calendar = f.variables['time'].calendar
except:
    raise
    sys.exit('Cannot read in the time calendar. Check if time variable has a calendar. If not, edit this file manually accordingly to suit your needs.')
try:
    time_date = nc4.num2date(time, time_units, time_calendar)                           
except:
    raise
    sys.exit('Cannot convert the time variable into actual dates, Check if the calendar supplied in the netCDF file is acceptable to the netCDF4.num2date function')
try:
    time_stmp = []
    for i in time_date:
	time_stmp.append(i.strftime('%Y%m%d%H%M'))
    time_stmp = np.array(time_stmp)
except:
    raise
    sys.exit('Cannot convert the dates to year_month_date_hour_minutes format')

try:
    var = f.variables[inputVar][:,:,:]
except:
    raise
    sys.exit('Cannot read %s variable from netCDF file. Please check that variable exists and has 3 dimensions (time, lat, lon). NOTE time is first dimension' %inputVar)

try:
    if 'Latitude' in tb_results.columns:
        if 'Longitude' in tb_results.columns:
            pass
        else:
            sys.exit('There is no column called "Longitude" in input csv file.')
    else:
        sys.exit('There is no column called "Latitude" in input csv file.')
except:
    raise
    sys.exit()
try:
    for i in time_stmp:
        tb_results[i] = np.nan
except:
    raise
    sys.exit()
try:
    for i in tb_results.index:
	mylon = tb_results.loc[i,'Longitude']
	mylat = tb_results.loc[i,'Latitude']
	# select number from the list with minimum distance from point/
	lattarget = min(lat, key=lambda x:abs(x-mylat))
	lontarget = min(lon, key=lambda x:abs(x-mylon))
	tb_results.loc[i, time_stmp] = var[:, np.argwhere(lat == lattarget), np.argwhere(lon == lontarget)].flatten()
except:
    raise
    sys.exit('Issues with extracting the cell nearest to the point. See errors raised')
    
try:
    tb_results.to_csv(outputcsv, index = False)
except:
    raise
    sys.exit('Cannot output results to CSV')
try:
    f.close()
except:
    raise
    sys.exit('Some problems with closing the netCDF file. Should not affect the results anyways')
