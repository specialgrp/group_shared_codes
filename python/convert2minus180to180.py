#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Oct  8 14:55:01 2018

@author: Kamolphat Atsawawaranunt

This code to automatically convert 360 degrees netCDF to -180 to 180 format.
This code works with netCDF file with variables up to 7 dimensions.
In cases where the netCDF file has variables more than 7 dimensions, the 
longitudes are converted to -180 to 180 but not resorted.

"""

import netCDF4 as nc4
import sys
import numpy as np

input_file = sys.argv[1]
lon_name = sys.argv[2]
output_file = sys.argv[3]

def convert2minus180to180(input_file, lon_name, output_file):
    """Convert netCDF file from 360 degrees to -180 to 180 degrees longitude format
    
    Check if netCDF file is 360 degrees format and convert to -180 to 180. 
    Then looks for whether the longitude are already in order (low to high 
    values). If the longitude is not in order, this is reordered and also all
    the variables which has longitude as one of the dimension. This code only 
    works with netCDF files with variables up to 7 dimensions. 
    
    Args:
        input_file: string, path to input netCDF file.
        lon_name:   string, name of longitude variable (i.e. 'lon', or 'longitude')
    
    Returns:
        None. The function edits the input netCDF file. It is to be noted that 
        if the function fails, there is a chance of the netCDF file being 
        corrupted. If the user's files contain variables with more than 7 
        dimensions, the longitudes will not be reordered (which may cause 
        some problem when using NCO to subset certain areas).
    
    Raise:
        ValueError: There are more than 7 dimensions in one of the variable. Longitude cannot be resorted automatically, and the original order is kept as is. Expand this code manually.

    """
    f = nc4.Dataset(input_file, 'r')
    lons = f.variables[lon_name][:]
    coord360 = False
    for i in lons:
        if i > 180:
            coord360 = True
            break
    if coord360:
        print("%s in 360 degrees longitude format. Converting to -180 to 180 format" %input_file)
        lons[lons[:]>180] = lons[:][lons[:]>180] - 360
    else:
        print("%s already in -180 to 180 degrees longitude format" %input_file)
    lon_idx = np.argsort(lons)
    if (lons == lons[lon_idx]).all():
        print("Longitude already in correct order, does not require resorting")
    else:
        print("Longitude not in correct order; resorting longitude and other variables with longitude dimension")
        var_list = f.variables.keys()
        warning = False
        for i in var_list:
            print(i)
            if len(f.variables[i].dimensions) > 7:
                warning = True
        if warning == True:
            f.close()
            raise ValueError('There are more than 7 dimensions in one of the variable. Longitude cannot be resorted automatically, and the original order is kept as is. Expand this code manually.')
        #output file
        outf = nc4.Dataset(output_file, "w")
        #Copy dimensions
        for dname, the_dim in f.dimensions.iteritems():
            outf.createDimension(dname, len(the_dim) if not the_dim.isunlimited() else None)
        # Copy variables
        for v_name, varin in f.variables.iteritems():
            outVar = outf.createVariable(v_name, varin.datatype, varin.dimensions)

            # Copy variable attributes
            outVar.setncatts({k: varin.getncattr(k) for k in varin.ncattrs()})
            if v_name == lon_name:
                pass
                #outVar[:] = lons
            else:
                outVar[:] = varin[:]
        outf.variables[lon_name][:] = lons[lon_idx]
        for i in var_list:
            if i != lon_name:
                # make sure to exclude lon variable to begin with
                f_dim = f.variables[i].dimensions
                if lon_name in f_dim:
                    # only work with variables with lon as a dimension
                    dim_idx = [j for j, v in enumerate(f_dim) if v == lon_name]
                    if len(dim_idx) != 1:
                        print('This conversion cannot be performed, there is something wrong with ' + i + ' variable')
                    else:
                        if len(f_dim) == 1:
                            outf.variables[i][:] = f.variables[i][lon_idx]
                            continue
                        if len(f_dim) == 2:
                            if dim_idx[0] == 0:
                                outf.variables[i][:] = f.variables[i][lon_idx,:]
                                continue
                            elif dim_idx[0] == 1:
                                outf.variables[i][:] = f.variables[i][:,lon_idx]
                                continue
                        elif len(f_dim) == 3:
                            if dim_idx[0] == 0:
                                outf.variables[i][:] = f.variables[i][lon_idx,:,:]
                                continue
                            elif dim_idx[0] == 1:
                                outf.variables[i][:] = f.variables[i][:,lon_idx,:]
                                continue
                            elif dim_idx[0] == 2:
                                outf.variables[i][:] = f.variables[i][:,:,lon_idx]
                                continue
                        elif len(f_dim) == 4:
                            if dim_idx[0] == 0:
                                outf.variables[i][:] = f.variables[i][lon_idx,:,:,:]
                                continue
                            elif dim_idx[0] == 1:
                                outf.variables[i][:] = f.variables[i][:,lon_idx,:,:]
                                continue
                            elif dim_idx[0] == 2:
                                outf.variables[i][:] = f.variables[i][:,:,lon_idx,:]
                                continue
                            elif dim_idx[0] == 3:
                                outf.variables[i][:] = f.variables[i][:,:,:,lon_idx]
                                continue
                        elif len(f_dim) == 5:
                            if dim_idx[0] == 0:
                                outf.variables[i][:] = f.variables[i][lon_idx,:,:,:,:]
                                continue
                            elif dim_idx[0] == 1:
                                outf.variables[i][:] = f.variables[i][:,lon_idx,:,:,:]
                                continue
                            elif dim_idx[0] == 2:
                                outf.variables[i][:] = f.variables[i][:,:,lon_idx,:,:]
                                continue
                            elif dim_idx[0] == 3:
                                outf.variables[i][:] = f.variables[i][:,:,:,lon_idx,:]
                                continue
                            elif dim_idx[0] == 4:
                                outf.variables[i][:] = f.variables[i][:,:,:,:,lon_idx]
                                continue
                        elif len(f_dim) == 6:
                            if dim_idx[0] == 0:
                                outf.variables[i][:] = f.variables[i][lon_idx,:,:,:,:,:]
                                continue
                            elif dim_idx[0] == 1:
                                outf.variables[i][:] = f.variables[i][:,lon_idx,:,:,:,:]
                                continue
                            elif dim_idx[0] == 2:
                                outf.variables[i][:] = f.variables[i][:,:,lon_idx,:,:,:]
                                continue
                            elif dim_idx[0] == 3:
                                outf.variables[i][:] = f.variables[i][:,:,:,lon_idx,:,:]
                                continue
                            elif dim_idx[0] == 4:
                                outf.variables[i][:] = f.variables[i][:,:,:,:,lon_idx,:]
                                continue
                            elif dim_idx[0] == 5:
                                outf.variables[i][:] = f.variables[i][:,:,:,:,:,lon_idx]
                                continue
                        elif len(f_dim) == 7:
                            if dim_idx[0] == 0:
                                outf.variables[i][:] = f.variables[i][lon_idx,:,:,:,:,:,:]
                                continue
                            elif dim_idx[0] == 1:
                                outf.variables[i][:] = f.variables[i][:,lon_idx,:,:,:,:,:]
                                continue
                            elif dim_idx[0] == 2:
                                outf.variables[i][:] = f.variables[i][:,:,lon_idx,:,:,:,:]
                                continue
                            elif dim_idx[0] == 3:
                                outf.variables[i][:] = f.variables[i][:,:,:,lon_idx,:,:,:]
                                continue
                            elif dim_idx[0] == 4:
                                outf.variables[i][:] = f.variables[i][:,:,:,:,lon_idx,:,:]
                                continue
                            elif dim_idx[0] == 5:
                                outf.variables[i][:] = f.variables[i][:,:,:,:,:,lon_idx,:]
                                continue
                            elif dim_idx[0] == 6:
                                outf.variables[i][:] = f.variables[i][:,:,:,:,:,:,lon_idx]
                                continue
                        else:
                            print('This should not happen. Warning should have been raised earlier. There are more than 7 dimensions in ' + i + '. Why so many dimensions? Expand this code manually')
    f.close()
    outf.close()

convert2minus180to180(input_file, lon_name, output_file)
