#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 12 10:20:35 2018

@author: Kamolphat Atsawawaranunt

This script is written to subset a netCDF file based on a shapefile

This script is written with ideas/functions from: 
https://github.com/pydata/xarray/issues/501

Outline
1. Import prerequisite modules
2. Define inputs
3. Define functions
4. Read in netCDF file
5. Read in shapefile
6. Convert shapefile to xarray DataArray
7. Mask out the input netCDF file based on the xarray DataArray from (6.)
8. Output netCDF file

"""

# =============================================================================
# 1. Import prerequisite modules
# =============================================================================
import geopandas
from rasterio import features
from affine import Affine
import numpy as np
import xray
import netCDF4 as nc4
import sys

# =============================================================================
# 2. Define inputs
# =============================================================================
#input_netcdf = 'input/lc_mod_2012.nc'
#varName = 'Landcover'
#input_shapefile = 'input/China_basemap/China_basemap/national boundary/bou1_4p.shp'
#output_netcdf = 'output1.nc'
#original_size = 'False'


if len(sys.argv) > 6:
    print('There are more than 5 arguments supplied. Only the first 5 arguments will be used')
elif len(sys.argv) == 5:
    print('original_size argument is missing. The default where the minimum netCDF size output will be performed instead (original_size = False)')
    original_size = 'False'
elif len(sys.argv) == 6:
    original_size = sys.argv[5]
elif len(sys.argv) < 5:
    sys.exit('There must be at least 4 arguments supplied. Not enough arguments supplied.')
else:
    pass

input_netcdf = sys.argv[1]
varName = sys.argv[2]
input_shapefile = sys.argv[3]
output_netcdf = sys.argv[4]

if original_size in ['True', 'true', 't', 'T']:
    original_size = True
elif original_size in ['False', 'false', 'f', 'F']:
    original_size = False
else:
    sys.exit('mean argument not correct; Either "True" or "False" in 5th argument')

# =============================================================================
# 3. Define functions
# =============================================================================
def transform_from_latlon(lat, lon):
    lat = np.asarray(lat)
    lon = np.asarray(lon)
    trans = Affine.translation(lon[0], lat[0])
    scale = Affine.scale(lon[1] - lon[0], lat[1] - lat[0])
    return trans * scale

def rasterize(shapes, coords, fill=np.nan, **kwargs):
    """Rasterize a list of (geometry, fill_value) tuples onto the given
    xray coordinates. This only works for 1d latitude and longitude
    arrays.
    """
    transform = transform_from_latlon(coords['latitude'], coords['longitude'])
    out_shape = (len(coords['latitude']), len(coords['longitude']))
    raster = features.rasterize(shapes, out_shape=out_shape,
                                fill=fill, transform=transform,
                                dtype=float, **kwargs)
    return xray.DataArray(raster, coords=coords, dims=('latitude', 'longitude'))

# =============================================================================
# 4. Read in netCDF file
# =============================================================================
try:
    f = nc4.Dataset(input_netcdf, 'r')
except:
    raise
    sys.exit('Cannot open netCDF file.')

try:
    vardim = f.variables[varName].dimensions
except:
    raise
    sys.exit('Cannot extract dimensions of variable: %s. Please make sure that variable = %s exists.' %(varName, varName))

if len(vardim)  > 5:
    sys.exit('There are more than 5 dimensions for variable: %s. Please edit the script manually if this is to be performed. Changes mainly to be made to section 7. and 8. of the script.')

try:
    if 'lon' in vardim:
        lonvar = 'lon'
    elif 'longitude' in vardim:
        lonvar = 'longitude'
    else:
        sys.exit('There is no obvious variable for longitude (lon or longitude).')
    lon = f.variables[lonvar][:]
except:
    sys.exit('Cannot read in the longitude variable, tried both "lon" and "longitude".')
    
try:
    if 'lat' in vardim:
        latvar = 'lat'
    elif 'latitude' in vardim:
        latvar = 'latitude'
    else:
        sys.exit('There is no obvious variable for latitude (lat or latitude).')
    lat = f.variables[latvar][:]
except:
    sys.exit('Cannot read in the latitude variable, tried both "lat" and "latitude".')

try:
    var = f.variables[varName][:]
except:
    raise
    sys.exit('Cannot read %s variable from netCDF file. Please check that variable exists' %varName)

# =============================================================================
# 5. Read in shapefile
# =============================================================================
shapefile = geopandas.read_file(input_shapefile)

# =============================================================================
# 6. Convert polygons to xarray DataArray
# =============================================================================
shapes = [(shape, n) for n, shape in enumerate(shapefile.geometry)]
# create xarray dataset based on the input latitude and longitude from the netCDF file
ds = xray.Dataset(coords={'longitude': lon,
                          'latitude': lat})
# rasterize all the shapes in the input shapefile. Whatever is outside the shapefile is given as np.nan
ds['shapes'] = rasterize(shapes, ds.coords, np.nan)

# =============================================================================
# 7. Mask out the input netCDF file based on the xarray DataArray from (6.)
# =============================================================================
mask = ds.shapes.data
mask[~np.isnan(mask)] = 1
lataxis = np.where(np.array(vardim) == latvar)[0][0]
lonaxis = np.where(np.array(vardim) == lonvar)[0][0]
if lataxis > lonaxis:
    mask = np.moveaxis(mask, 0, 1)
if len(vardim) == 2:
    var_output = var*mask
elif len(vardim) == 3:
    if ((lataxis == 0) | (lonaxis == 0)) :
        if ((lataxis == 1) | (lonaxis == 1)):
            var_output = var*mask[:,:,np.newaxis]
        elif ((lataxis == 2) | (lonaxis == 2)):
            var_output = var*mask[:,np.newaxis,:]
        else:
            sys.exit('this should be impossible, please check the script and the input file, and edit the input file manually.')
    elif ((lataxis == 1) | (lonaxis == 1)):
        if ((lataxis == 2) | (lonaxis == 2)):
            var_output = var*mask[np.newaxis,:,:]
        else:
            sys.exit('this should be impossible, please check the script and the input file, and edit the input file manually.')
    else:
        sys.exit('this should be impossible, please check the script and the input file, and edit the input file manually.')

elif len(vardim) == 4:
    if ((lataxis == 0) | (lonaxis == 0)) :
        if ((lataxis == 1) | (lonaxis == 1)):
            var_output = var*mask[:,:,np.newaxis,np.newaxis]
        elif ((lataxis == 2) | (lonaxis == 2)):
            var_output = var*mask[:,np.newaxis,:,np.newaxis]
        elif ((lataxis == 3) | (lonaxis == 3)):
            var_output = var*mask[:,np.newaxis,np.newaxis,:]
        else:
            sys.exit('this should be impossible, please check the script and the input file, and edit the input file manually.')
    elif ((lataxis == 1) | (lonaxis == 1)):
        if ((lataxis == 2) | (lonaxis == 2)):
            var_output = var*mask[np.newaxis,:,:,np.newaxis]
        elif ((lataxis == 3) | (lonaxis == 3)):
            var_output = var*mask[np.newaxis,:,np.newaxis,:]
    elif ((lataxis == 2) | (lonaxis == 2)):
        if ((lataxis == 3) | (lonaxis == 3)):
            var_output = var*mask[np.newaxis,np.newaxis,:,:]
        else:
            sys.exit('this should be impossible, please check the script and the input file, and edit the input file manually.')
    else:
        sys.exit('this should be impossible, please check the script and the input file, and edit the input file manually.')

elif len(vardim) == 5:
    if ((lataxis == 0) | (lonaxis == 0)) :
        if ((lataxis == 1) | (lonaxis == 1)):
            var_output = var*mask[:,:,np.newaxis,np.newaxis,np.newaxis]
        elif ((lataxis == 2) | (lonaxis == 2)):
            var_output = var*mask[:,np.newaxis,:,np.newaxis,np.newaxis]
        elif ((lataxis == 3) | (lonaxis == 3)):
            var_output = var*mask[:,np.newaxis,np.newaxis,:,np.newaxis]
        elif ((lataxis == 4) | (lonaxis == 4)):
            var_output = var*mask[:,np.newaxis,np.newaxis,np.newaxis,:]
        else:
            sys.exit('this should be impossible, please check the script and the input file, and edit the input file manually.')
    elif ((lataxis == 1) | (lonaxis == 1)) :
        if ((lataxis == 2) | (lonaxis == 2)):
            var_output = var*mask[np.newaxis,:,:,np.newaxis,np.newaxis]
        elif ((lataxis == 3) | (lonaxis == 3)):
            var_output = var*mask[np.newaxis,:,np.newaxis,:,np.newaxis]
        elif ((lataxis == 4) | (lonaxis == 4)):
            var_output = var*mask[np.newaxis,:,np.newaxis,np.newaxis,:]
        else:
            sys.exit('this should be impossible, please check the script and the input file, and edit the input file manually.')
    elif ((lataxis == 2) | (lonaxis == 2)):
        if ((lataxis == 3) | (lonaxis == 3)):
            var_output = var*mask[np.newaxis,np.newaxis,:,:,np.newaxis]
        elif ((lataxis == 4) | (lonaxis == 4)):
            var_output = var*mask[np.newaxis,np.newaxis,:,np.newaxis,:]
    elif ((lataxis == 3) | (lonaxis == 3)):
        if ((lataxis == 4) | (lonaxis == 4)):
            var_output = var*mask[np.newaxis,np.newaxis,np.newaxis,:,:]
        else:
            sys.exit('this should be impossible, please check the script and the input file, and edit the input file manually.')
    else:
        sys.exit('this should be impossible, please check the script and the input file, and edit the input file manually.')
else:
    sys.exit('There are more than 5 dimensions for variable: %s. Please edit the script manually if this is to be performed.')

# =============================================================================
# 8. Output netCDF file
# =============================================================================
#output file
dsout = nc4.Dataset(output_netcdf, "w")
if original_size:
    #Copy dimensions
    for dname, the_dim in f.dimensions.iteritems():
    #    print dname, len(the_dim)
        print(dname)
        dsout.createDimension(dname, len(the_dim) if not the_dim.isunlimited() else None)
    
    # Copy variables
    for v_name, varin in f.variables.iteritems():
        outVar = dsout.createVariable(v_name, varin.datatype, varin.dimensions)
    #    print varin.datatype
        print v_name
        
        # Copy variable attributes
        outVar.setncatts({k: varin.getncattr(k) for k in varin.ncattrs()})
        if v_name == varName:
            outVar[:] = var_output[:]
        else:
            outVar[:] = varin[:]
        # close the output file
else:
    print('Minimum size netCDF file will be output (just covering area with values after cropping)')
    lat_idx = np.arange(np.min(np.where(~np.isnan(mask))[0]), np.max(np.where(~np.isnan(mask))[0]) + 1)
    lon_idx = np.arange(np.min(np.where(~np.isnan(mask))[1]), np.max(np.where(~np.isnan(mask))[1]) + 1)
    lat = lat[lat_idx]
    lon = lon[lon_idx]
    if len(vardim) == 2:
        if lataxis == 0:
            var_output = var_output[lat_idx, :]
            var_output = var_output[:, lon_idx]
        else:
            var_output = var_output[:, lat_idx]
            var_output = var_output[lon_idx, :]
    elif len(vardim) == 3:
        if lataxis == 0:
            var_output = var_output[lat_idx, :, :]
        elif lataxis == 1:
            var_output = var_output[:, lat_idx, :]
        elif lataxis == 2:
            var_output = var_output[:, :, lat_idx]
        else:
            sys.exit('this should be impossible, please check the script and the input file, and edit the input file manually.')
        if lonaxis == 0:
            var_output = var_output[lon_idx, :, :]
        elif lonaxis == 1:
            var_output = var_output[:, lon_idx, :]
        elif lonaxis == 2:
            var_output = var_output[:, :, lon_idx]
        else:
            sys.exit('this should be impossible, please check the script and the input file, and edit the input file manually.')
    elif len(vardim) == 4:
        if lataxis == 0:
            var_output = var_output[lat_idx, :, :, :]
        elif lataxis == 1:
            var_output = var_output[:, lat_idx, :, :]
        elif lataxis == 2:
            var_output = var_output[:, :, lat_idx, :]
        elif lataxis == 3:
            var_output = var_output[:, :, :, lat_idx]
        else:
            sys.exit('this should be impossible, please check the script and the input file, and edit the input file manually.')
        if lonaxis == 0:
            var_output = var_output[lon_idx, :, :, :]
        elif lonaxis == 1:
            var_output = var_output[:, lon_idx, :, :]
        elif lonaxis == 2:
            var_output = var_output[:, :, lon_idx, :]
        elif lonaxis == 3:
            var_output = var_output[:, :, :, lon_idx]
        else:
            sys.exit('this should be impossible, please check the script and the input file, and edit the input file manually.')
    elif len(vardim) == 5:
        if lataxis == 0:
            var_output = var_output[lat_idx, :, :, :, :]
        elif lataxis == 1:
            var_output = var_output[:, lat_idx, :, :, :]
        elif lataxis == 2:
            var_output = var_output[:, :, lat_idx, :, :]
        elif lataxis == 3:
            var_output = var_output[:, :, :, lat_idx, :]
        elif lataxis == 4:
            var_output = var_output[:, :, :, :, lat_idx]
        else:
            sys.exit('this should be impossible, please check the script and the input file, and edit the input file manually.')
        if lonaxis == 0:
            var_output = var_output[lon_idx, :, :, :, :]
        elif lonaxis == 1:
            var_output = var_output[:, lon_idx, :, :, :]
        elif lonaxis == 2:
            var_output = var_output[:, :, lon_idx, :, :]
        elif lonaxis == 3:
            var_output = var_output[:, :, :, lon_idx, :]
        elif lonaxis == 4:
            var_output = var_output[:, :, :, :, lon_idx]
        else:
            sys.exit('this should be impossible, please check the script and the input file, and edit the input file manually.')
    else:
        sys.exit('There are more than 5 dimensions for variable: %s. Please edit the script manually if this is to be performed.')

    #Copy dimensions
    for dname, the_dim in f.dimensions.iteritems():
    #    print dname, len(the_dim)
        if dname == lonvar:
            dsout.createDimension(dname, len(lon))
        elif dname == latvar:
            dsout.createDimension(dname, len(lat))
        else:
            dsout.createDimension(dname, len(the_dim) if not the_dim.isunlimited() else None)
    
    # Copy variables
    for v_name, varin in f.variables.iteritems():
        outVar = dsout.createVariable(v_name, varin.datatype, varin.dimensions)
    #    print varin.datatype
        print v_name
        
        # Copy variable attributes
        outVar.setncatts({k: varin.getncattr(k) for k in varin.ncattrs()})
        if v_name == varName:
            outVar[:] = var_output[:]
        elif v_name == lonvar:
            outVar[:] = lon[:]
        elif v_name == latvar:
            outVar[:] = lat[:]
        else:
            outVar[:] = varin[:]
        
# close the output file
dsout.close()
# close the input file
f.close()
