# -*- coding: utf-8 -*-
"""
Created on Tue Jul  4 14:01:38 2017

@author: Kamolphat Atsawawaranunt

This script is to create time series plot

The time units are not converted to dates due to the large possibilities of time units
i.e. 'seconds from date_time', 'months from xxxx', 'Years CE', 'hours from date_time', etc.

"""
# Import prerequisite modules
import netCDF4 as nc4
import numpy as np
from glob import glob
import pandas as pd
import math
import matplotlib.pyplot as plt
import sys

# Input variables from command line/ terminal
input_file = sys.argv[1]
var_name = sys.argv[2] #'tas'
outputcsv = sys.argv[3] #'output.csv'
outputpdf = sys.argv[4] #'output.pdf'

# Open netCDF file
print('opening file')
f = nc4.Dataset(input_file, 'r')
print('file opened')
time = f.variables['time'][:]
time_units = f.variables['time'].units
try:
    time_calendar = f.variables['time'].calendar
    time_units = time_units + ', ' + time_calendar + ' calendar'
except:
    print('no time calendar in %s' %input_file)
try:
    lons = f.variables['longitude'][:]
except:
    print("some error, longitude variable not called 'longitude', trying 'lon';")
    lons = f.variables['lon'][:]
try:
    lats = f.variables['latitude'][:]
except:
    print("some error, latitude variable not called 'latitude', trying 'lat';")
    lats = f.variables['lat'][:]
var = f.variables[var_name][:]
var_units = f.variables[var_name].units
time_len = len(time)
main = var

# Define area weighted mean function
def area_weighted_mean_2_list(weighted_matrix, var):
    """weighted matrix and var must share same dimension

    Note that NA is ignored here.

    returns list

    """
    ann_tas_ls = []
    time_idx = range(0, var.shape[0]) # length of time dimension. In the case that array is made with dstack, use last dimension instead of 1st
    for i in time_idx:
        ann_tas = var[i,:,:]
        ann_tas_ls.append(np.nansum(ann_tas*weighted_matrix)/(np.sum(weighted_matrix[np.isfinite(ann_tas)])))
    return(ann_tas_ls)



# Create universal weight matrix
lats_mat = []
for i in lats:
    lats_mat.append(math.cos(math.radians(i)))
lats_mat = np.repeat(lats_mat, len(lons), axis = 0)
lats_mat = lats_mat.reshape(len(lats), len(lons))

# Calculate area weighted mean. 
ann_tas_ls = area_weighted_mean_2_list(lats_mat, main)

# Output to CSV and PDF
d = {time_units: time,
     'area_weighted_mean': ann_tas_ls}
tb = pd.DataFrame(data = d)
tb.to_csv(outputcsv, index = False)

plt.title('Drift examinations for ' + var_name)
plt.figure(figsize = (20,8))
plt.axhline(y = np.mean(tb['area_weighted_mean']), label = 'mean', color = 'r', linestyle = '-')
plt.plot(tb[time_units], tb['area_weighted_mean'], label=var_name, color = 'b')
plt.ylabel(var_units)
plt.xlabel(time_units)
plt.legend()
plt.savefig(outputpdf)
plt.close()
